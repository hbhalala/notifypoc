package com.ilex.notify.event.incoming;

import com.ilex.notify.event.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EmailHandler implements MessageHandler {

  @Autowired EventGenerator eventGenerator;

  @Override
  public void handleMessage(Message<?> message) throws MessagingException {
    MessageHeaders headers = message.getHeaders();
    log.debug(
        "ContentType: "
            + headers.get(MessageHeaders.CONTENT_TYPE)
            + ", mail_from: "
            + headers.get("mail_from")
            + ", mail_subject: "
            + headers.get("mail_subject")
            + ", mail_contentType: "
            + headers.get("mail_contentType")
            + ", payload: "
            + message.getPayload().toString());

    eventGenerator.generate(
        Event.builder()
            .subject(headers.get("mail_subject").toString())
            .message("//TODO " + "(Content Conversion Pending):" + headers.get("mail_contentType"))
            .userId(emailToUserName(headers.get("mail_from").toString()))
            .build());
  }

  private String emailToUserName(String email) {
    // TODO - map email to usernames
    return email;
  }
}
