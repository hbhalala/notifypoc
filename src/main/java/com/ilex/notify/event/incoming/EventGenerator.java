package com.ilex.notify.event.incoming;

import com.ilex.notify.event.Event;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EventGenerator {

  @Autowired private Queue queue;

  @Autowired private RabbitTemplate template;

  public void generate(Event event) {
    log.debug("Generating event: " + event.toString());
    this.template.convertAndSend(queue.getName(), event);
  }
}
