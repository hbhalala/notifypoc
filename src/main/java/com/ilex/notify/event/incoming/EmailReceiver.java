package com.ilex.notify.event.incoming;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.context.IntegrationFlowContext;
import org.springframework.integration.mail.dsl.Mail;
import org.springframework.integration.mail.support.DefaultMailHeaderMapper;
import org.springframework.stereotype.Component;

@Component
@EnableIntegration
@IntegrationComponentScan
public class EmailReceiver {

  @Autowired EmailHandler emailHandler;

  @Autowired private IntegrationFlowContext flowContext;

  public void startEmailReceiver(String user, String pw) {
    IntegrationFlow flow =
        IntegrationFlows.from(
                Mail.imapIdleAdapter(imapUrl(user, pw))
                    .javaMailProperties(p -> p.put("mail.debug", "false"))
                    .headerMapper(new DefaultMailHeaderMapper()))
            .handle(emailHandler)
            .get();
    this.flowContext.registration(flow).register();
  }

  private String imapUrl(String user, String pw) {
    // TODO these needs to be configurable
    String url = "imaps://" + user + ":" + pw + "@imap.gmail.com:" + 993 + "/INBOX";
    return url;
  }
}
