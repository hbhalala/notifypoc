package com.ilex.notify.event;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface EventRepository extends MongoRepository<Event, String> {
  // TODO add method declatation as needed to acccess mongo data
}
