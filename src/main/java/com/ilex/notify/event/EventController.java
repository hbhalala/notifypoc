package com.ilex.notify.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/events")
public class EventController {

  @Autowired EventRepository eventRepository;

  @RequestMapping(method = RequestMethod.GET)
  public List<Event> get() {
    return eventRepository.findAll();
  }

  @RequestMapping(method = RequestMethod.POST)
  public HttpStatus post(@RequestBody Event event) {
    // TODO add input validation
    eventRepository.insert(event);
    return HttpStatus.OK;
  }

  @RequestMapping(method = RequestMethod.PUT)
  public HttpStatus put(@RequestBody Event event) {
    // TODO add input validation
    eventRepository.save(event);
    return HttpStatus.OK;
  }
}
