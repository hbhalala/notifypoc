package com.ilex.notify.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RabbitListener(queues = "events")
public class EventProcessor {

  @Autowired
  EventRepository eventRepository;

  @RabbitHandler
  public void processMessage(Event event) {
    log.debug("Received event from AMQP: " + event.toString());
    eventRepository.insert(event);
    // TODO - continue further processing of this event

  }
}
