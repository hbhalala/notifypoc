package com.ilex.notify.event;

public enum EventStatus {
  PENDING_APPROVAL,
  APPROVED,
  PENDING_DELIVERY,
  DELIVERED,
  COMPLETED
}
