package com.ilex.notify;

import com.ilex.notify.event.incoming.EmailReceiver;
import com.ilex.notify.event.incoming.EventGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.integration.config.EnableIntegration;

@SpringBootApplication
@EnableIntegration
public class NotifyApplication {

  @Autowired EventGenerator eventGenerator;

  @Autowired EmailReceiver emailReceiver;

  public static void main(String[] args) {
    SpringApplication.run(NotifyApplication.class, args);
  }

  // TODO - needs to refactor so that Spring can control it
  @EventListener(ApplicationReadyEvent.class)
  public void emailReceiver() {
    // TODO - credentials are to be configurable
    emailReceiver.startEmailReceiver("ilexpoc%40gmail.com", "ilexpoc1");
  }
}
