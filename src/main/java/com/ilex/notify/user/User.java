package com.ilex.notify.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Data
@Document
public class User {
  @Id String id;
  String role;

  @Indexed(name = "userName", unique = true)
  String userName;

  String password; // TODO ensure this is not sent to the user
  String firstName;
  String lastName;
  String email;
  String phone;
  List<String> groupIds;
}
