package com.ilex.notify.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@Slf4j
public class UserController {

  @Autowired UserRepository userRepository;

  @Autowired BCryptPasswordEncoder passwordEncoder;

  @RequestMapping(method = RequestMethod.GET)
  public List<User> get() {
    return userRepository.findAll();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{userName}")
  public User getWithUserName(@PathVariable("userName") String userName) {
    return userRepository.findByUserName(userName);
  }

  @RequestMapping(method = RequestMethod.POST)
  public HttpStatus post(@RequestBody User user) {
    // TODO add input validation
    // TODO encode the password before insert
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.insert(user);
    return HttpStatus.CREATED;
  }

  @RequestMapping(method = RequestMethod.PUT)
  public HttpStatus put(@RequestBody User user) {
    // TODO add input validation
    // TODO handle change password request
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    userRepository.save(user);
    return HttpStatus.OK;
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{userName}")
  public HttpStatus deleteWithUserName(@PathVariable("userName") String userName) {
    userRepository.deleteByUserName(userName);
    return HttpStatus.OK;
  }
}
