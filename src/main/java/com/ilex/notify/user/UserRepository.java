package com.ilex.notify.user;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
  public User findByUserName(String userName);

  public void deleteByUserName(String userName);
}
