package com.ilex.notify.group;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupRepository extends MongoRepository<Group, String> {
  Group findByGroupName(String groupName);

  void deleteByGroupName(String groupName);
}
