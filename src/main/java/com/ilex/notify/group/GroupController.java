package com.ilex.notify.group;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/groups")
@Slf4j
public class GroupController {

  @Autowired GroupRepository groupRepository;

  @RequestMapping(method = RequestMethod.GET)
  public List<Group> get() {
    return groupRepository.findAll();
  }

  @RequestMapping(method = RequestMethod.GET, value = "/{groupName}")
  public Group getWithGroupName(@PathVariable("groupName") String groupName) {
    return groupRepository.findByGroupName(groupName);
  }

  @RequestMapping(method = RequestMethod.POST)
  public HttpStatus post(@RequestBody Group group) {
    // TODO add input validation
    groupRepository.insert(group);
    return HttpStatus.CREATED;
  }

  @RequestMapping(method = RequestMethod.PUT)
  public HttpStatus put(@RequestBody Group group) {
    // TODO add input validation
    groupRepository.save(group);
    return HttpStatus.OK;
  }

  @RequestMapping(method = RequestMethod.DELETE, value = "/{groupName}")
  public HttpStatus deleteWithUserName(@PathVariable("groupName") String groupName) {
    groupRepository.deleteByGroupName(groupName);
    return HttpStatus.OK;
  }
}
