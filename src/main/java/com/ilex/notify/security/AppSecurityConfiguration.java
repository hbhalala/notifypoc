package com.ilex.notify.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class AppSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired private BasicAuthenticationPoint basicAuthenticationPoint;

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.csrf()
        .disable()
        .authorizeRequests()
        .anyRequest()
        .authenticated()
        .and()
        .httpBasic()
        .authenticationEntryPoint(basicAuthenticationPoint);
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    // TODO currently only one user is configured, add authentication for all users
    // TODO move it to database instead of inMemory
    auth.inMemoryAuthentication()
        .withUser("hitesh")
        .password("$2a$04$rPnD9QcEnrKeGej94xQ49.D9mMqeBwbYrH03dgJhLhclHSNz1V4KK")
        .roles("USER");
  }
}
