package com.ilex.notify;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(
  classes = {NotifyApplication.class},
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class NotifyApplicationTests {

  @Autowired private TestRestTemplate restTemplate;

  @Test
  public void
      GIVEN_a_rest_end_point_WHEN_request_is_sent_without_authentication_VERIFY_that_the_access_is_denied() {

    // GIVEN WHEN
    String body = this.restTemplate.getForObject(TestConstants.API_URL_USERS, String.class);

    // VERIFY
    assertEquals(true, body.startsWith(HttpStatus.UNAUTHORIZED.toString()));
  }
}
