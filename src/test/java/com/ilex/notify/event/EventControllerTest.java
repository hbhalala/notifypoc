package com.ilex.notify.event;

import com.ilex.notify.BaseTest;
import com.ilex.notify.TestConstants;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EventControllerTest extends BaseTest {

  @Test
  public void GIVEN_an_event_WHEN_post_rest_end_point_is_called_VERIFY_that_the_event_is_created() {

    // GIVEN
    String name = getRandomName();
    Event event =
        Event.builder()
            .subject("Field Trip Reminder!")
            .message("Reminder for Frield Trip next week.")
            .userId(name)
            .status(EventStatus.APPROVED)
            .build();
    // WHEN
    ResponseEntity<Event> postResponse = create(TestConstants.API_URL_EVENTS, event, Event.class);

    // VERIFY
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    Event gotGroup = getFromUrl(url, Event.class);
    assertEquals(name, gotGroup.getUserId());

    delete(url);
  }

  @Test
  public void
      GIVEN_a_modified_event_WHEN_put_rest_end_point_is_called_VERIFY_that_the_event_is_updated() {

    // GIVEN
    String name = getRandomName();
    Event event =
        Event.builder()
            .subject("Field Trip Reminder!")
            .message("Reminder for Frield Trip next week.")
            .userId(name)
            .status(EventStatus.APPROVED)
            .build();
    ResponseEntity<Event> postResponse = create(TestConstants.API_URL_EVENTS, event, Event.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    Event gotEvent = getFromUrl(url, Event.class);
    gotEvent.setSubject("Field Trip Changes!");

    // WHEN
    update(url, gotEvent);

    // VERIFY
    Event gotEventAgain = getFromUrl(url, Event.class);
    assertEquals("Field Trip Changes!", gotEventAgain.getSubject());

    delete(url);
  }

  @Test
  public void
      GIVEN_an_event_url_WHEN_delete_rest_end_point_is_called_VERIFY_that_the_event_is_deleted() {

    // GIVEN
    String name = getRandomName();
    Event event =
        Event.builder()
            .subject("Field Trip Reminder!")
            .message("Reminder for Frield Trip next week.")
            .userId(name)
            .status(EventStatus.APPROVED)
            .build();
    ResponseEntity<Event> postResponse = create(TestConstants.API_URL_EVENTS, event, Event.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);

    // WHEN
    delete(url);

    // VERIFY
    Event shouldBeNull = getFromUrl(url, Event.class);
    assertEquals(null, shouldBeNull);
  }
}
