package com.ilex.notify.user;

import com.ilex.notify.BaseTest;
import com.ilex.notify.TestConstants;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class UserControllerTest extends BaseTest {

  @Test
  public void GIVEN_a_user_WHEN_post_rest_end_point_is_called_VERIFY_that_the_user_is_created() {

    // GIVEN
    String userName = getRandomName();
    User user =
        User.builder()
            .userName(userName)
            .role("PARENT")
            .firstName("John")
            .lastName("Snow")
            .email(userName + "@wakanda.com")
            .phone("1234567890")
            .build();

    // WHEN
    ResponseEntity<User> postResponse = create(TestConstants.API_URL_USERS, user, User.class);

    // VERIFY
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    User gotUser = getFromUrl(url, User.class);
    assertEquals(userName, gotUser.getUserName());
    assertEquals("PARENT", gotUser.getRole());
    assertEquals("John", gotUser.getFirstName());
    assertEquals("Snow", gotUser.getLastName());
    assertEquals(userName + "@wakanda.com", gotUser.getEmail());
    assertEquals("1234567890", gotUser.getPhone());

    delete(url);
  }

  @Test
  public void
      GIVEN_a_modified_user_WHEN_put_rest_end_point_is_called_VERIFY_that_the_user_is_updated() {

    // GIVEN
    String userName = getRandomName();
    User user =
        User.builder()
            .userName(userName)
            .role("PARENT")
            .firstName("John")
            .lastName("Snow")
            .email(userName + "@wakanda.com")
            .phone("1234567890")
            .build();
    ResponseEntity<User> postResponse = create(TestConstants.API_URL_USERS, user, User.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    User gotUser = getFromUrl(url, User.class);
    gotUser.setPhone("0987654321");

    // WHEN
    update(url, gotUser);

    // VERIFY
    User gotUserAgain = getFromUrl(url, User.class);
    assertEquals("0987654321", gotUserAgain.getPhone());

    delete(url);
  }

  @Test
  public void
      GIVEN_a_user_url_WHEN_delete_rest_end_point_is_called_VERIFY_that_the_user_is_deleted() {

    // GIVEN
    String userName = getRandomName();
    User user =
        User.builder()
            .userName(userName)
            .role("PARENT")
            .firstName("John")
            .lastName("Snow")
            .email(userName + "@wakanda.com")
            .phone("1234567890")
            .build();
    ResponseEntity<User> postResponse = create(TestConstants.API_URL_USERS, user, User.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);

    // WHEN
    delete(url);

    // VERIFY
    User shouldBeNull = getFromUrl(url, User.class);
    assertEquals(null, shouldBeNull);
  }
}
