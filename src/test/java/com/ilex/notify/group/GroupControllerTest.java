package com.ilex.notify.group;

import com.ilex.notify.BaseTest;
import com.ilex.notify.TestConstants;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GroupControllerTest extends BaseTest {

  @Test
  public void GIVEN_a_group_WHEN_post_rest_end_point_is_called_VERIFY_that_the_group_is_created() {

    // GIVEN
    String name = getRandomName();
    Group group = Group.builder().groupName(name).description("test group").build();

    // WHEN
    ResponseEntity<Group> postResponse = create(TestConstants.API_URL_GROUPS, group, Group.class);

    // VERIFY
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    Group gotGroup = getFromUrl(url, Group.class);
    assertEquals(name, gotGroup.getGroupName());
    assertEquals("test group", gotGroup.getDescription());

    delete(url);
  }

  @Test
  public void
      GIVEN_a_modified_group_WHEN_put_rest_end_point_is_called_VERIFY_that_the_group_is_updated() {

    // GIVEN
    String name = getRandomName();
    Group group = Group.builder().groupName(name).description("test group").build();
    ResponseEntity<Group> postResponse = create(TestConstants.API_URL_GROUPS, group, Group.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);
    Group gotGroup = getFromUrl(url, Group.class);
    gotGroup.setDescription("new description");

    // WHEN
    update(url, gotGroup);

    // VERIFY
    Group gotGroupAgain = getFromUrl(url, Group.class);
    assertEquals("new description", gotGroupAgain.getDescription());

    delete(url);
  }

  @Test
  public void
      GIVEN_a_group_url_WHEN_delete_rest_end_point_is_called_VERIFY_that_the_group_is_deleted() {

    // GIVEN
    String name = getRandomName();
    Group group = Group.builder().groupName(name).description("test group").build();
    ResponseEntity<Group> postResponse = create(TestConstants.API_URL_GROUPS, group, Group.class);
    assertEquals(HttpStatus.CREATED.value(), postResponse.getStatusCodeValue());
    String url = extractUrlFromResponse(postResponse);
    assertNotNull(url);

    // WHEN
    delete(url);

    // VERIFY
    Group shouldBeNull = getFromUrl(url, Group.class);
    assertEquals(null, shouldBeNull);
  }
}
