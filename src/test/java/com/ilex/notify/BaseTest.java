package com.ilex.notify;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(
  classes = {NotifyApplication.class},
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class BaseTest {

  private final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  private final int RANDOM_STRING_LENGTH = 16;

  @Autowired protected TestRestTemplate restTemplate;

  protected String randomAlphaNumeric(int count) {
    StringBuilder builder = new StringBuilder();
    while (count-- != 0) {
      int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
      builder.append(ALPHA_NUMERIC_STRING.charAt(character));
    }
    return builder.toString();
  }

  protected String getRandomName() {
    return randomAlphaNumeric(RANDOM_STRING_LENGTH);
  }

  protected void delete(String url) {
    this.restTemplate.withBasicAuth(TestConstants.USERNAME, TestConstants.PASSWORD).delete(url);
  }

  protected <T> T getFromUrl(String url, Class<T> responseType) {
    return this.restTemplate
        .withBasicAuth(TestConstants.USERNAME, TestConstants.PASSWORD)
        .getForObject(url, responseType);
  }

  protected <T> void update(String url, T entity) {
    this.restTemplate
        .withBasicAuth(TestConstants.USERNAME, TestConstants.PASSWORD)
        .put(url, entity);
  }

  protected <T> ResponseEntity<T> create(String api, T entity, Class<T> responseType) {
    return this.restTemplate
        .withBasicAuth(TestConstants.USERNAME, TestConstants.PASSWORD)
        .postForEntity(api, entity, responseType);
  }

  protected <T> String extractUrlFromResponse(ResponseEntity<T> response) {
    return response.getHeaders().get(HttpHeaders.LOCATION).get(0);
  }
}
